package homework5;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Boss extends Thread{
  private int attack;
  private String bossName;
  private double attackSpeed;
  private int healthPoints;

  /* Between 0 / 100 */
  private int massDamageChance;
  private ArrayList<Chicken> chickens = new ArrayList<>();

  public Boss() {

  }

  public Boss(int attack, double attackSpeed, int healthPoints, int massDamageChance, String bossName) {
    this.attack = attack;
    this.attackSpeed = attackSpeed;
    this.healthPoints = healthPoints;
    this.massDamageChance = massDamageChance;
    this.bossName = bossName;
  }

  @Override
  public void run() {
    while (healthPoints > 0 && chickens.stream().anyMatch((chicken) -> chicken.getHealthPoints() > 0)) {
      if (generateInt(100) > massDamageChance) {
        chickens.forEach((chicken -> chicken.setHealthPoints(chicken.getHealthPoints() - attack)));
        chickens.forEach((chicken) -> {
          if (chicken.getHealthPoints() <= 0) {
            chicken.killChicken();
            try {
              chicken.join();
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            System.out.println("Boss just smashed " + chicken.getChickenName());
          }
        });
        chickens = chickens.stream().filter((chicken) -> chicken.getHealthPoints() > 0).collect(Collectors.toCollection(ArrayList::new));

        System.out.println("The boss just hit everybody for " + attack);
      } else {
        Chicken chicken = chickens.get(generateInt(chickens.size() - 1));
        chicken.setHealthPoints(chicken.getHealthPoints() - (attack - chicken.getArmour()));

        if (chicken.getHealthPoints() <= 0) {
          chicken.killChicken();
          System.out.println("The boss just killed " + chicken.getChickenName());
          try {
            chicken.join();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } else {
          System.out.println("Boss just hit " + chicken.getChickenName() + " for " + (attack - chicken.getArmour()));
        }
      }

      try {
        sleep((long) (1000 / attackSpeed));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    if (healthPoints > 0) {
      System.out.println("Boss: EZ");
    } else {
      System.out.println("Boss: Lag");
    }
  }

  public String getBossName() {
    return bossName;
  }

  public void addTarget(Chicken chicken) {
    chickens.add(chicken);
  }

  public void setBossName(String bossName) {
    this.bossName = bossName;
  }

  private int generateInt(int max) {
    return (int) (Math.random() * ((max) + 1));
  }

  public int getAttack() {
    return attack;
  }

  public void setAttack(int attack) {
    this.attack = attack;
  }

  public double getAttackSpeed() {
    return attackSpeed;
  }

  public void setAttackSpeed(double attackSpeed) {
    this.attackSpeed = attackSpeed;
  }

  public int getHealthPoints() {
    return healthPoints;
  }

  public void setHealthPoints(int healthPoints) {
    this.healthPoints = healthPoints;
  }

  public int getMassDamageChance() {
    return massDamageChance;
  }

  public void setMassDamageChance(int massDamageChance) {
    this.massDamageChance = massDamageChance;
  }

  public ArrayList<Chicken> getChickens() {
    return chickens;
  }

  public void setChickens(ArrayList<Chicken> chickens) {
    this.chickens = chickens;
  }
}
