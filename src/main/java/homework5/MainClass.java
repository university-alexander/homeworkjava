package homework5;

public class MainClass {

	public static void main(String[] args) {
		Chicken c1 = new Chicken("Kokoshka 1", 20, 300, 90, 30, 40, 5, 4);
		Chicken c2 = new Chicken("Kokoshka 2", 30, 700, 30, 10, 10, 10, 2);
		Boss boss = new Boss(40, 2, 10000, 30, "Pesho");
		boss.addTarget(c1);
		boss.addTarget(c2);
		c1.setTarget(boss);
		c2.setTarget(boss);
		
		c1.start();
		c2.start();
		boss.start();

	}

}
