package homework5;

import java.util.Random;

public class Chicken extends Thread {

	private volatile boolean chickenIsAlive;
	private String chickenName;
	private int dmg;
	private int healthPoints;
	private int mana;
	private int magicDmg;
	private int magicChance;
	private int armour;
	private double attackSpeed;
	private final Random random = new Random();
	private Boss target;
	
	public Chicken(String chickenName, int dmg, int healthPoints, int mana, int magicDmg, int magicChance, int armour, double attackSpeed) {
		this.chickenName = chickenName;
		this.dmg = dmg;
		this.healthPoints = healthPoints;
		this.mana = mana;
		this.magicDmg = magicDmg;
		this.magicChance = magicChance;
		this.armour = armour;
		this.attackSpeed = attackSpeed;
	}

	public Chicken() {

	}

	@Override
	public void run() {
		this.chickenIsAlive = true;
		while(this.chickenIsAlive){
			target.setHealthPoints(target.getHealthPoints() - (dmg));;

			System.out.println(this.chickenName + " just hit  "
					+ target.getBossName() + " for " + (dmg) +
					 " and left it with " + target.getHealthPoints());
			
			if(mana > 15 && random.nextInt(100) <= magicChance) {
				mana -= 15;
				target.setHealthPoints(target.getHealthPoints() - magicDmg);
				System.out.println(chickenName + " made magic dmg "
						+ " and left " + target.getBossName() +
						" with " + target.getHealthPoints() + " hp.");
			}else {
				mana += mana * 0.1;
			}
			
			try {
				sleep((long) (1000 / attackSpeed));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
		}
		
		if(healthPoints > 0) {
			System.out.println(chickenName + ": HAHAHAH you will make "
					+ "great chicken nugets!");
		}else {
			System.out.println(chickenName + ": Cheater!!!!!!!!!!!!");
		}
	}

	public void killChicken() {
		this.chickenIsAlive = false;
	}

	public int getDmg() {
		return dmg;
	}

	public void setDmg(int dmg) {
		this.dmg = dmg;
	}

	public int getHealthPoints() {
		return healthPoints;
	}

	public void setHealthPoints(int healthPoints) {
		this.healthPoints = healthPoints;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public int getMagicDmg() {
		return magicDmg;
	}

	public void setMagicDmg(int magicDmg) {
		this.magicDmg = magicDmg;
	}

	public int getMagicChance() {
		return magicChance;
	}

	public void setMagicChance(int magicChance) {
		this.magicChance = magicChance;
	}

	public int getArmour() {
		return armour;
	}

	public void setArmour(int armour) {
		this.armour = armour;
	}

	public double getAttackSpeed() {
		return attackSpeed;
	}

	public void setAttackSpeed(double attackSpeed) {
		this.attackSpeed = attackSpeed;
	}

	public void setTarget(Boss target) {
		this.target = target;
	}

	public String getChickenName() {
		return chickenName;
	}

	public void setChickenName(String chickenName) {
		this.chickenName = chickenName;
	}
}
