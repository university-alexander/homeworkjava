package leetcode;

public class MaximumProductOfWordLengths {

  private boolean checkForCommonChar(String word1, String word2) {
    for (Character char1 : word1.toCharArray()) {
      for (Character char2 : word2.toCharArray()) {
        if (char1 == char2) {
          return true;
        }
      }
    }

    return false;
  }

  public int maxProduct(String[] words) {
    int maxProduct = 0;

    for (int i = 0; i < words.length; i++) {
      String outerWord = words[i];

      for (int j = 0; j < words.length; j++) {
        if (i == j) {
          continue;
        }
        String innerWord = words[j];
        int currentProduct = outerWord.length() * innerWord.length();
        if (checkForCommonChar(outerWord, innerWord)) {
          maxProduct = Math.max(currentProduct, maxProduct);
        }
      }
    }

    return maxProduct;
  }
}
