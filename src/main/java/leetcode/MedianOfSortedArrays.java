package leetcode;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class MedianOfSortedArrays {

  public double findMedianSortedArrays(int[] nums1, int[] nums2) {
    double median;

    List<Integer> list2 = Stream.concat(Arrays.stream(nums1).boxed(), Arrays.stream(nums2).boxed()).toList();
    list2.sort(Integer::compareTo);

    int size = list2.size();

    if (size % 2 == 0) {
      median = (list2.get(size / 2) + list2.get((size / 2) + 1)) >> 1;
    } else {
      median = list2.get(size / 2);
    }

    return median;
  }
}
