package exampleexam;

public class Person {
  private String name;
  private Gender gender;
  private String id;

  public Person(String name, Gender gender, String id) {
    this.name = name;
    this.gender = gender;
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Gender getGender() {
    return gender;
  }

  public void setGender(Gender gender) {
    this.gender = gender;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
