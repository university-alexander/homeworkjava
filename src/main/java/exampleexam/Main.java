package exampleexam;

import static java.text.MessageFormat.format;

public class Main {

  public static void main(String[] args) {
    CreateData createData = new CreateData();
    BusinessLogic businessLogic = new BusinessLogic();

    System.out.println(businessLogic.findStudentWithHighestGrade(createData.getStudents(), Gender.MALE));
    System.out.println(format("Average salary is {0}", businessLogic.calculateAverageSalary(createData.getProfessors())));
  }
}