package exampleexam;

import static java.text.MessageFormat.format;

public class Student extends Person{

  private Float averageGrade;

  public Student(String name, Gender gender, String id, Float averageGrade) {
    super(name, gender, id);
    this.averageGrade = averageGrade;
  }

  public Float getAverageGrade() {
    return averageGrade;
  }

  public void setAverageGrade(Float averageGrade) {
    this.averageGrade = averageGrade;
  }

  @Override
  public String toString() {
    return format("Student with name {0} is {1} with id {2} and average {3} grade.", getName(), getGender().gender, getId(), getAverageGrade());
  }
}
