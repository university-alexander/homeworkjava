package exampleexam;

public enum Gender {
  FEMALE("female"),
  MALE("male");

  public final String gender;

  Gender(String gender) {
    this.gender = gender;
  }
}