package exampleexam;

public class Professor extends Person{
  private int salary;

  public Professor(String name, Gender gender, String id, int salary) {
    super(name, gender, id);
    this.salary = salary;
  }

  public int getSalary() {
    return salary;
  }

  public void setSalary(int salary) {
    this.salary = salary;
  }
}
