package exampleexam;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.text.MessageFormat.format;

public class CreateData {
  private final String[] firstNames = new String[] { "Trevor", "Marni", "Nusaybah", "Amelia-Rose", "Trey", "Wilma", "Richie", "Kamil", "Connor", "Roberta" };
  private final String[] lastNames = new String[] { "Garrison", "Turner", "Stubbs", "Pickett", "Ridley", "Matthews", "Hume", "Valentine", "Morrow", "Tapia" };
  private final ArrayList<String> studentIds = new ArrayList<>(
          List.of("0307292008", "2512015143", "4312222390", "4504178980", "9505303690", "0251116431", "0812019172", "6311025872", "7901179853", "9012185376")
  );
  private final ArrayList<String> professorIds = new ArrayList<>(
          List.of("0505307934", "1010112530", "2907145358", "4603122697", "6802183615", "0507169161", "1602067326", "2208088621", "2508154920", "3205307440")
  );
  private final ArrayList<Student> students = new ArrayList<>();
  private final ArrayList<Professor> professors = new ArrayList<>();

  public CreateData() {
    generateProfessors();
    generateStudents();
  }

  public ArrayList<Student> getStudents() {
    return students;
  }

  public ArrayList<Professor> getProfessors() {
    return professors;
  }

  private String getRandomFirstName() {
    return firstNames[generateInt(0, firstNames.length - 1)];
  }

  private String getRandomLastName() {
    return lastNames[generateInt(0, lastNames.length - 1)];
  }

  private int generateInt(int min, int max) {
    return min + (int)(Math.random() * ((max - min) + 1));
  }

  private float randomGradeGenerator() {
    return new Random().nextFloat() * (6.0f - 2.0f) + 2.0f;
  }

  private String getRandomFullName() {
    return format("{0} {1}", getRandomFirstName(), getRandomLastName());
  }

  private void generateStudents() {
    studentIds.forEach((studentId) -> students.add(new Student(getRandomFullName(), Gender.values()[generateInt(0, 1)], studentId, randomGradeGenerator())));
  }

  private void generateProfessors() {
    professorIds.forEach((professorId) -> professors.add(new Professor(getRandomFullName(), Gender.values()[generateInt(0, 1)], professorId, generateInt(1000, 4000))));
  }
}