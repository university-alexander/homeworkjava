package exampleexam;

import java.util.ArrayList;
import java.util.Comparator;

public class BusinessLogic {

  public Student findStudentWithHighestGrade(ArrayList<Student> students, Gender gender) {
    return students
            .stream()
            .filter((student) -> student.getGender() == gender)
            .sorted(Comparator.comparing(Student::getAverageGrade))
            .toList()
            .get(0);
  }

  public float calculateAverageSalary(ArrayList<Professor> professors) {
    float averageSalary = 0;
    for (Professor professor : professors) {
      averageSalary += professor.getSalary();
    }

    return averageSalary / professors.size();
  }
}
