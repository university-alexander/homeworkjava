package homework123;

import java.util.Arrays;
import java.util.Scanner;
import static java.text.MessageFormat.format;

public class Triangle {

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    int[] triangleSides = getUserInput();

    if (checkForTriangle(triangleSides)) {
      System.out.println(format("This triangle is {0} and is {1}", getTriangleType(triangleSides), isRectangular(triangleSides)));
    } else {
      System.out.println("Such triangle can't exist.");
    }
  }

  private static int[] getUserInput() {

    int[] triangleSides = new int[3];

    for (int i = 0; i < 3; i++) {
      System.out.println(format("Please enter the {0} side of the triangle", numberToWord(i)));
      triangleSides[i] = scanner.nextInt();
    }

    return triangleSides;
  }

  private static String numberToWord(int number) {
    switch (number) {
      case 0 -> { return "AB"; }
      case 1 -> { return "BC"; }
      case 2 -> { return "CA"; }
      default -> { return ""; }
    }
  }

  private static boolean checkForTriangle(int[] triangleSides) {
    return triangleSides[0] + triangleSides[1] > triangleSides[2]
            && triangleSides[0] + triangleSides[2] > triangleSides[1]
            && triangleSides[1] + triangleSides[2] > triangleSides[0];
  }

  private static String getTriangleType(int[] triangleSides) {
    if (Arrays.stream(triangleSides).allMatch((side) -> side == triangleSides[0])) {
      return "equilateral";
    } else if (Arrays.stream(triangleSides).distinct().count() == triangleSides.length) {
      return "versatile";
    } else {
      return "isosceles";
    }
  }

  private static String isRectangular(int[] triangleSides) {
    return square(triangleSides[0]) + square(triangleSides[1]) == square(triangleSides[2]) ? "rectangular" : "not rectangular";
  }

  private static int square(int number) {
    return (int) (Math.pow(number, 2));
  }
}


