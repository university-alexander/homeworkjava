package homework123;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class FibonacciNumbers {
  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    double userInput = getUserInput();

    printFibonacciNormal(userInput);
    printFibonacciRecursive(userInput);
  }

  private static void printFibonacciNormal(double number) {
    ArrayList<Double> startingNumbers = new ArrayList<>(Arrays.asList(0.0, 1.0, 1.0));
    for (int i = 3; i < number; i++) {
      startingNumbers.add(startingNumbers.get(i - 1) + startingNumbers.get(i - 2));
    }

    startingNumbers.forEach(System.out::println);
  }

  private static double printFibonacciRecursive(double number)
  {
    if (number <= 1) {
      return number;
    }
    System.out.println(number);
    return printFibonacciRecursive(number - 1) + printFibonacciRecursive(number - 2);
  }

  private static double getUserInput() {
    System.out.println("To which fibonacci number do you want?");
    return scanner.nextDouble();
  }
}