package homework123;

import java.util.ArrayList;
import java.util.Scanner;
import static java.text.MessageFormat.format;

public class AppWithMenu {
  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {

    int action;
    do {
      printMenu();
      action = scanner.nextInt();
      runAction(action);
    } while (action != 0);
  }

  private static void printMenu() {

    System.out.println("Choose one of the following actions:");
    System.out.println("0: Exit the program.");
    System.out.println("1: Perfect numbers.");
    System.out.println("2: Interval sum and average.");
    System.out.println("*******************************");
  }

  private static void runAction(int action) {
    switch (action) {
      case 1 -> findPerfectNumbers();
      case 2 -> intervalSumAndAverage();
      default -> System.exit(0);
    }
  }

  private static void findPerfectNumbers() {
    int sum = 0;
    int[] interval = getInterval();
    ArrayList<Integer> perfectNumbers = new ArrayList<>();

    for (int i = interval[0]; i <= interval[1]; i++) {
      for (int j = 1; j < i; j++) {
        if (i % j == 0) {
          sum += j;
        }
      }

      if (sum == i) {
        perfectNumbers.add(i);
      }

      sum = 0;
    }

    System.out.println(format("These are the perfect numbers {0}", perfectNumbers));
  }

  private static void intervalSumAndAverage() {

    float sum = 0;
    int counter = 0;
    int[] interval = getInterval();

    for (int i = interval[0]; i <= interval[1]; i++) {
      sum += i;
      counter++;
    }

    System.out.println(format("The sum of all the numbers in the array is {0} and the average value of the numbers is {1}", sum, sum / counter));
  }

  /**
   * Tuk predpolagam che intervala shte e pravilno vuveden zatova ne pravq
   * nikakvi proverki
   * @return the interval the user wants
   */
  private static int[] getInterval() {

    int[] interval = new int[2];

    System.out.println("Please enter the first number of the interval.");
    interval[0] = scanner.nextInt();

    System.out.println("Please enter the first number of the interval.");
    interval[1] = scanner.nextInt();

    return interval;
  }
}
