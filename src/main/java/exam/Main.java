package exam;

import java.util.ArrayList;

import static java.text.MessageFormat.format;

public class Main {
  public static void main(String[] args) {
    BusinessLogic businessLogic = new BusinessLogic();

    ArrayList<Room> rooms = new ArrayList<>();
    rooms.add(new Room(3, 20, true));
    rooms.add(new Room(2, 10, false));
    rooms.add(new Room(1, 8, true));

    ArrayList<Apartment> apartments = new ArrayList<>();
    apartments.add(new Apartment(5, 120, true, ApartmentType.NORMAL));
    apartments.add(new Apartment(3, 90, false, ApartmentType.NORMAL));
    apartments.add(new Apartment(7, 160, true, ApartmentType.PRESIDENT));

    Room biggestRoom = businessLogic.findBiggestRoom(rooms);
    Apartment smallestApartment = businessLogic.findSmallestApartment(apartments);

    System.out.println(format("The biggest room has {0} square meters area.", biggestRoom.getArea()));
    System.out.println(format("The smallest apartment has {0} square meters area.", smallestApartment.getArea()));
    System.out.println(businessLogic.biggerOfTheTwo(biggestRoom.getArea(), smallestApartment.getArea()));
  }
}
