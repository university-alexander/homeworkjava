package exam;

import java.util.ArrayList;
import java.util.Comparator;

public class BusinessLogic {

  public Room findBiggestRoom(ArrayList<Room> rooms) {
    return rooms.stream().sorted(Comparator.comparingInt(Room::getArea)).toList().get(rooms.size() - 1);
  }

  public Apartment findSmallestApartment(ArrayList<Apartment> apartments) {
    return apartments.stream().sorted(Comparator.comparingInt(Apartment::getArea)).toList().get(0);
  }

  public String biggerOfTheTwo(int roomArea, int apartmentArea) {
    return roomArea > apartmentArea ? "Room is bigger" : "Apartment is bigger";
  }
}
