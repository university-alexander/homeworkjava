package exam;

public class Premises {
  private int numberOfBeds;
  private int area;

  public Premises(int numberOfBeds, int area) {
    this.numberOfBeds = numberOfBeds;
    this.area = area;
  }

  public int getNumberOfBeds() {
    return numberOfBeds;
  }

  public void setNumberOfBeds(int numberOfBeds) {
    this.numberOfBeds = numberOfBeds;
  }

  public int getArea() {
    return area;
  }

  public void setArea(int area) {
    this.area = area;
  }
}
