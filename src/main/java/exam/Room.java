package exam;

import static java.text.MessageFormat.format;

public class Room extends Premises {

  private boolean hasBalcony;

  public Room(int numberOfBeds, int area, boolean hasBalcony) {
    super(numberOfBeds, area);
    this.hasBalcony = hasBalcony;
  }

  public boolean isHasBalcony() {
    return hasBalcony;
  }

  public void setHasBalcony(boolean hasBalcony) {
    this.hasBalcony = hasBalcony;
  }

  private String hasBalconyToString() {
    return hasBalcony ? "" : "not";
  }

  @Override
  public String toString() {
    return format("This room is {0} square meters area and does {1} have a balcony.", getArea(), hasBalconyToString());
  }
}
