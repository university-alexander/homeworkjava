package exam;

public enum ApartmentType {
  PRESIDENT("president"),
  NORMAL("normal");

  public final String apartmentType;

  ApartmentType(String apartmentType) {
    this.apartmentType = apartmentType;
  }
}
