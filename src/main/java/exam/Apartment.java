package exam;

import static java.text.MessageFormat.format;

public class Apartment extends Premises {

  private boolean hasBath;
  private ApartmentType apartmentType;

  public Apartment(int numberOfBeds, int area, boolean hasBath, ApartmentType apartmentType) {
    super(numberOfBeds, area);
    this.hasBath = hasBath;
    this.apartmentType = apartmentType;
  }

  public boolean isHasBath() {
    return hasBath;
  }

  public void setHasBath(boolean hasBath) {
    this.hasBath = hasBath;
  }

  public ApartmentType getApartmentType() {
    return apartmentType;
  }

  public void setApartmentType(ApartmentType apartmentType) {
    this.apartmentType = apartmentType;
  }

  private String hasBathToString() {
    return hasBath ? "" : "not";
  }

  @Override
  public String toString() {
    return format("This {0} apartment is {1} square meters area and does {2} have a bath.", apartmentType, getArea(), hasBathToString());
  }
}
