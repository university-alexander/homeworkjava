package homework4;

import java.sql.Timestamp;
import java.util.Date;

public class Main {
  public static void main(String[] args) {

    Student student = new Student("Alex", 20, "2001681007", "STD");
    System.out.println(student);

    Employee employee = new Employee("Alex", 20, "2001681007", new Timestamp(System.currentTimeMillis()));
    System.out.println(employee);

    Faculty faculty = new Faculty("Alex", 20, "2001681007", new Timestamp(System.currentTimeMillis()), Position.ASSISTANT);
    System.out.println(faculty);

    Staff staff = new Staff("Alex", 20, "2001681007", new Timestamp(System.currentTimeMillis()), Position.SECRETARY);
    System.out.println(staff);
  }
}
