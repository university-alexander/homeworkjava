package homework4;

public enum Position {
  ASSISTANT("Assistant"),
  ASSOCIATE_PROFESSOR("Associate Professor"),
  PROFESSOR("Professor"),
  SECRETARY("Secretary");


  private final String textRepresentation;

  Position(String textRepresentation) {
    this.textRepresentation = textRepresentation;
  }

  @Override
  public String toString() {
    return textRepresentation;
  }
}
