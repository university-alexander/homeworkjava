package homework4;

import static java.text.MessageFormat.format;

public class Student extends Person{

  private String studentId;
  private String courseName;

  public Student(String name, int age, String studentId, String courseName) {
    super(name, age);
    this.studentId = studentId;
    this.courseName = courseName;
  }

  public String getStudentId() {
    return studentId;
  }

  public void setStudentId(String studentId) {
    this.studentId = studentId;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  @Override
  public String toString() {
    return format("{0} studies {1} and has {2} student id", super.toString(), courseName, studentId);
  }
}
