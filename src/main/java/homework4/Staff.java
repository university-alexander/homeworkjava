package homework4;

import java.sql.Timestamp;

import static java.text.MessageFormat.format;

public class Staff extends Employee{
  private Position position;

  public Staff(Position position) {
    this.position = position;
  }

  public Staff(String name, int age, String personalId, Timestamp joinDate, Position position) {
    super(name, age, personalId, joinDate);
    this.position = position;
  }

  public Position getPosition() {
    return position;
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  @Override
  public String toString() {
    return format("{0} and is working as {1}", super.toString(), position.toString());
  }
}
