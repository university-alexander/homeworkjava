package homework4;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import static java.text.MessageFormat.format;

public class Employee extends Person{

  private String personalId;
  private Timestamp joinDate;

  public Employee() {
    super();
  }

  public Employee(String name, int age, String personalId, Timestamp joinDate) {
    super(name, age);
    this.personalId = personalId;
    this.joinDate = joinDate;
  }

  public String getPersonalId() {
    return personalId;
  }

  public void setPersonalId(String personalId) {
    this.personalId = personalId;
  }

  public Timestamp getJoinDate() {
    return joinDate;
  }

  public void setJoinDate(Timestamp joinDate) {
    this.joinDate = joinDate;
  }

  @Override
  public String toString() {
    return format("{0} joined on {1} with this personal id {2}", super.toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm").format(joinDate), personalId);
  }
}
